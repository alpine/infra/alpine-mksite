# Donate to Alpine Linux

If you would like to donate to Alpine Linux, please use one of the options on our [Open Collective](https://opencollective.com/alpinelinux).
Your support will help cover costs for infrastructure, and other essential expenses.
More information can be read on our Open Collective [post](/posts/2025-01-30-alpine-linux-joins-open-collective.html).

Thank you for your support!

<br>

# Supporters

<script src="https://opencollective.com/alpinelinux/banner.js"></script>
