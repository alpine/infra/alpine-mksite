---
title: 'Alpine 3.12.11, 3.13.9, 3.14.5 and 3.15.3 released'
date: 2022-03-28
---

Alpine 3.12.11, 3.13.9, 3.14.5 and 3.15.3 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of the releases: 

- [3.12.11](https://git.alpinelinux.org/aports/log/?h=v3.12.11)
- [3.13.9](https://git.alpinelinux.org/aports/log/?h=v3.13.9)
- [3.14.5](https://git.alpinelinux.org/aports/log/?h=v3.14.5)
- [3.15.3](https://git.alpinelinux.org/aports/log/?h=v3.15.3)

These releases include a security fix for zlib's [CVE-2018-25032](https://security.alpinelinux.org/vuln/CVE-2018-25032).
