---
title: 'Alpine 3.18.11, 3.19.6, 3.20.5 and 3.21.2 released'
date: 2025-01-08
---

Alpine 3.18.11, 3.19.6, 3.20.5 and 3.21.2 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of new stable releases:

- [3.18.11](https://git.alpinelinux.org/aports/log/?h=v3.18.11)
- [3.19.6](https://git.alpinelinux.org/aports/log/?h=v3.19.6)
- [3.20.5](https://git.alpinelinux.org/aports/log/?h=v3.20.5)
- [3.21.2](https://git.alpinelinux.org/aports/log/?h=v3.21.2)

These releases include a regression fix for
[ca-certificates](https://gitlab.alpinelinux.org/alpine/ca-certificates/-/issues/6).

