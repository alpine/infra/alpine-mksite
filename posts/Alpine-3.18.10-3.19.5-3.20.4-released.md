---
title: 'Alpine 3.18.10, 3.19.5 and 3.20.4 released'
date: 2025-01-06
---

Alpine 3.18.10, 3.19.5 and 3.20.4 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of new stable releases:

- [3.18.10](https://git.alpinelinux.org/aports/log/?h=v3.18.10)
- [3.19.5](https://git.alpinelinux.org/aports/log/?h=v3.19.5)
- [3.20.4](https://git.alpinelinux.org/aports/log/?h=v3.20.4)

Those releases contains various security fixes including a fix for OpenSSL
[CVE-2024-9143](https://security.alpinelinux.org/vuln/CVE-2024-9143).
