---
title: 'Alpine 3.17.8, 3.18.7 and 3.19.2 released'
date: 2024-06-18
---

Alpine 3.17.8, 3.18.7 and 3.19.2 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of new stable releases:

- [3.17.8](https://git.alpinelinux.org/aports/log/?h=v3.17.8)
- [3.18.7](https://git.alpinelinux.org/aports/log/?h=v3.18.7)
- [3.19.2](https://git.alpinelinux.org/aports/log/?h=v3.19.2)

Those releases contains various security fixes including fixes for:

## OpenSSL

- [CVE-2024-2511](https://security.alpinelinux.org/vuln/CVE-2024-2511)
- [CVE-2024-4603](https://security.alpinelinux.org/vuln/CVE-2024-4603)

## busybox

- [CVE-2023-42363](https://security.alpinelinux.org/vuln/CVE-2023-42363)
- [CVE-2023-42364](https://security.alpinelinux.org/vuln/CVE-2023-42364)
- [CVE-2023-42365](https://security.alpinelinux.org/vuln/CVE-2023-42365)
- [CVE-2023-42366](https://security.alpinelinux.org/vuln/CVE-2023-42366)

