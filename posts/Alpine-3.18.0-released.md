---
title: 'Alpine 3.18.0 released'
date: 2023-05-09
---

Alpine Linux 3.18.0 Released
============================

We are pleased to announce the release of Alpine Linux 3.18.0, the first in
the v3.18 stable series.

<a name="highlights">Highlights</a>
----------

* Linux kernel [6.1](https://kernelnewbies.org/Linux_6.1) - with signed kernel modules
* musl libc [1.2.4](https://www.openwall.com/lists/musl/2023/05/02/1) - now
  with TCP fallback in DNS resolver
* Python [3.11](https://docs.python.org/3/whatsnew/3.11.html)
* Ruby [3.2](https://www.ruby-lang.org/en/news/2022/12/25/ruby-3-2-0-released/)
* Node.js (current) [20.1](https://nodejs.org/en/blog/release/v20.1.0/)
* GNOME [44](https://release.gnome.org/44/)
* Go [1.20](https://go.dev/blog/go1.20)
* KDE Plasma [5.27](https://kde.org/announcements/plasma/5/5.27.0/)
* Rust [1.69](https://blog.rust-lang.org/2023/04/20/Rust-1.69.0.html)
* Experimental support for unattended installs via
  [tiny-cloud](https://gitlab.alpinelinux.org/alpine/cloud/tiny-cloud)


<a name="significant_changes">Significant changes</a>
-------------------

Kernel modules are now
[signed](https://www.kernel.org/doc/html/v4.15/admin-guide/module-signing.html).
Verified modules are not enforced by default, so 3rd party modules with
[akms](https://github.com/jirutka/akms) still works. 

All packages for ppc64le, x86, and x86_64 was linked with
[DT_RELR](https://gitlab.alpinelinux.org/alpine/tsc/-/issues/58). This should
have reduced size of compiled binares.

Python pre-compiled files (`pyc`) are now shipped in separate packages. It is now
possible to avoid install those and save space by doing `apk add !pyc`.

<a name="upgrade_notes">Upgrade notes</a>
-------------

As always, make sure to use `apk upgrade --available` when switching between
  major versions.

<a name="changes">Changes</a>
-------

The full list of changes can be found in the [wiki][7],  [git log][8] and [bug tracker][9].

<a name="credits">Credits</a>
-------

Thanks to everyone sending patches, bug reports, new and updated aports,
and to everyone helping with writing documentation, maintaining the
infrastructure, or contributing in any other way!

Thanks to [GIGABYTE][1], [Linode][2], [Fastly][3], [IBM][4], [Equinix Metal][5],
[vpsFree][6] and [ungleich][10] for providing us with hardware and hosting.

[1]: https://www.gigabyte.com/
[2]: https://linode.com
[3]: https://www.fastly.com/
[4]: https://ibm.com/
[5]: https://www.equinix.com/
[6]: https://vpsfree.org
[7]: https://wiki.alpinelinux.org/wiki/Release_Notes_for_Alpine_3.18.0
[8]: https://git.alpinelinux.org/cgit/aports/log/?h=v3.18.0
[9]: https://gitlab.alpinelinux.org/alpine/aports/issues?scope=all&utf8=%E2%9C%93&state=closed&milestone_title=3.18.0
[10]: https://ungleich.ch/

### aports Commit Contributors

<pre>
6543
AN3223
Adam Jensen
Adam Plumb
Adam Thiede
Aiden Grossman
Alex Denes
Alex Dowad
Alex McGrath
Alex Xu (Hello71)
Alexander Edland
Alexey Minnekhanov
Alexey Yerin
Andres Almiray
André Klitzing
Andy Hawkins
Andy Postnikov
Anjandev Momi
Antoine Martin
Antoni Aloy Torrens
Apo Apangona
Ariadne Conill
Armin Weigl
Arnav Singh
Bader Zaidan
Bart Ribbers
Ben Fuhrmannek
Bradford D. Boyle
Brian Cole
Brice
Bryan Kaplan
Camil Băncioiu
Carlo Landmeter
Catherine Schönhammer
Clayton Craft
Coco Liliace
Conrad Hoffmann
Consus
Cormac Stephenson
Cowington Post
Craig Andrews
Cédric Bellegarde
Daniel Fancsali
Dave Henderson
David Demelier
David Florness
David Wilson
Dekedro
Dermot Bradley
Dhruvin Gandhi
Djaker Abderrahmane
Dmitry Zakharchenko
Dominika Liberda
Dominique Martinet
DracoBlue
Drew DeVault
Duncan Bellamy
Dylan Van Assche
Dzmitry Sankouski
Edd Salkield
Eirik Furuseth
Elagost
Elly Fong-Jones
Eloi Torrents
Emma Nora Theuer
Fabricio Silva
Fiona Klute
FollieHiyuki
Francesco Colista
G.J.R Timmer
Galen Abell
Gary Holtz
Geod24
Glenn Strauss
Grigory Kirillov
Guillaume Quintard
Guy Broome
Guy Godfroy
Haelwenn (lanodan) Monnier
Henrik Grimler
Henrik Riomar
Hiroshi Kajisha
Hoang Nguyen
Holger Jaekel
Hoël Bézier
Hugo Osvaldo Barrera
Hugo Rodrigues
Hugo Wang
Hygna
Iskren Chernev
Iztok Fister Jr
Izumi Tsutsui
J0WI
Jacopo Mondi
Jake Buchholz Göktürk
Jakob Hauser
Jakob Meier
Jakub Jirutka
Jakub Panek
Jami Kettunen
Jason Swank
Jeff Dickey
Jeremy Saklad
Jinming Wu, Patrick
Johannes Heimansberg
John Gebbie
John Unland
John Vogel
Jonas
Jonas Heinrich
Jonas Marklén
Jordan Christiansen
Jordan ERNST
Josef Vybíhal
JuniorJPDJ
KAAtheWise
Kaarle Ritvanen
Karel Gardas
Kaspar Schleiser
Kate
Kay Thomas
Kevin Daudt
Klaus Frank
Klemens Nanni
Konstantin Kulikov
Krassy Boykinov
Krystian Chachuła
Laszlo Gombos
Lauren N. Liberda
Laurent Bercot
Lauri Tirkkonen
Leon Marz
Leon ROUX
Leonardo Arena
Linux User
Luca Weiss
Lucas Ramage
Lucidiot
MPThLee
Maarten van Gompel
Magnus Sandin
Marc0x1
Marco Schröder
Marian Buschsieweke
Mark Hills
Martijn Braam
Matthew T Hoare
Matthew Via
Maxim Karasev
Michael Ekstrand
Michael Pirogov
Michal Jirku
Michal Tvrznik
Michal Vasilek
Michał Polański
Mike Crute
Milan P. Stanić
Miles Alan
Natanael Copa
Nathan Angelacos
Newbyte
Nico de Haer
Nicolas Lorin
Noah Zalev
Noel Kuntze
Nulo
Oleg Titov
Oliver Smith
Olliver Schinagl
Orhun Parmaksız
Pablo Correa Gómez
Patrick Gansterer
Paul Bredbury
Peter Shkenev
Peter van Dijk
Petr Hodina
Petr Vorel
Philipp Arras
Piraty
ProgCat
Prokop Randacek
Quillith
R4SAS
Ralf Rachinger
Raymond Hackley
Rob Blanckaert
Rogério da Silva Yokomizo
Rosie K Languet
Rudolf Polzer
Saijin-Naib
Sascha Brawer
Sean McAvoy
Severin Neumann
Simon Frankenberger
Simon Rupf
Simon Zeni
Siva Mahadevan
Sodface
Stacy Harper
Stefan de Konink
Steffen Nurpmeso
Steve McMaster
Steven Honson
Stone Tickle
Sylvain Prat
Síle Ekaterin Liszka
Sören Tempel
Thomas Aldrian
Thomas Deutsch
Thomas Faughnan
Thomas Kienlen
Thomas Liske
TianYaX
Tim Magee
Tim Stanley
Timo Teräs
Timotej Lazar
Timothy Legge
Tom Lebreux
Tom Wieczorek
Uli Baum
Umar Getagazov
Valentin
Veikka Valtteri Kallinen
Wesley van Tilburg
Will Sinatra
William Desportes
William Walker
Willow Barraco
Wolf
Yuriy Chumak
Ziyao
alealexpro100
alice
build@apk-groulx
crapStone
dixyes
donoban
eugenefil
firefly-cpp
guddaff
j.r
knuxify
kpcyrd
leso-kn
lgehr
macmpi
messense
mio
nibon7
nu
omni
ovf
prspkt
psykose
ptrcnull
sales@ip2location.com
strophy
techknowlogick
tetsumaki
wdl
wener
xrs
Óliver García Albertos
</pre>
