---
title: 'Alpine 3.17.9, 3.18.8 and 3.19.3 released'
date: 2024-07-22
---

Alpine 3.17.9, 3.18.8 and 3.19.3 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of new stable releases:

- [3.17.9](https://git.alpinelinux.org/aports/log/?h=v3.17.9)
- [3.18.8](https://git.alpinelinux.org/aports/log/?h=v3.18.8)
- [3.19.3](https://git.alpinelinux.org/aports/log/?h=v3.19.3)

Those releases contains various security fixes including a fix for OpenSSL
[CVE-2024-5535](https://security.alpinelinux.org/vuln/CVE-2024-5535).

