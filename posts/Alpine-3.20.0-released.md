---
title: 'Alpine 3.20.0 released'
date: 2024-05-22
---

Alpine Linux 3.20.0 Released
============================

We are pleased to announce the release of Alpine Linux 3.20.0, the first in
the v3.20 stable series.

<a name="highlights">Highlights</a>
----------

* LLVM [18](https://releases.llvm.org/18.1.0/docs/ReleaseNotes.html)
* Node.js (lts) [20.10](https://nodejs.org/en/blog/release/v20.13.0)
* Python [3.12](https://www.python.org/downloads/release/python-3120/)
* Ruby [3.3](https://www.ruby-lang.org/en/news/2023/12/25/ruby-3-3-0-released/)
* Rust [1.78](https://blog.rust-lang.org/2024/05/02/Rust-1.78.0.html)
* Crystal [1.12](https://github.com/crystal-lang/crystal/releases/tag/1.12.0)
* GNOME [46](https://release.gnome.org/46/)
* Go [1.22](https://go.dev/blog/go1.22)
* KDE [6](https://kde.org/announcements/megarelease/6/)
* Sway [1.9](https://github.com/swaywm/sway/releases/tag/1.9)
* .NET [8.0](https://learn.microsoft.com/en-us/dotnet/core/whats-new/dotnet-8/overview)

<a name="significant_changes">Significant changes</a>
-------------------

Initial support for 64 bit RISC-V was added.


<a name="upgrade_notes">Upgrade notes</a>
-------------

As always, make sure to use `apk upgrade --available` when switching between
  major versions.

The `yq` package was [renamed](https://gitlab.alpinelinux.org/alpine/aports/-/issues/16052) to `yq-go`.

<a name="changes">Changes</a>
-------

The full list of changes can be found in the [wiki][1],  [git log][2] and [bug tracker][3].

<a name="credits">Credits</a>
-------

Thanks to everyone sending patches, bug reports, new and updated aports,
and to everyone helping with writing documentation, maintaining the
infrastructure, or contributing in any other way!

Thanks to [GIGABYTE][4], [Linode][5], [Fastly][6], [IBM][7], [Equinix Metal][8],
[vpsFree][9] and [ungleich][10] for providing us with hardware and hosting.

[1]: https://wiki.alpinelinux.org/wiki/Release_Notes_for_Alpine_3.20.0
[2]: https://git.alpinelinux.org/cgit/aports/log/?h=v3.20.0
[3]: https://gitlab.alpinelinux.org/alpine/aports/issues?scope=all&utf8=%E2%9C%93&state=closed&milestone_title=3.20.0
[4]: https://www.gigabyte.com/
[5]: https://linode.com
[6]: https://www.fastly.com/
[7]: https://ibm.com/
[8]: https://www.equinix.com/
[9]: https://vpsfree.org
[10]: https://ungleich.ch/

### aports Commit Contributors

<pre>
6543
Aaron Fischer
Adam Jensen
Adam Thiede
Adrian Siekierka
Adrián Arroyo Calle
Affe Null
Aleks Bunin
Alex Denes
Alex McGrath
Alex Xu (Hello71)
Alexey Minnekhanov
Alexey Yerin
Alistair Francis
Amelia Clarke
Andre Klitzing
Andrei Jiroh Eugenio Halili
Andrej Kolchin
Andres Almiray
André Klitzing
André Zwing
Andy Hawkins
Andy Postnikov
Anjandev Momi
Antoine Martin
Anton Bambura
Antoni Aloy Torrens
Ariadne Conill
Arnav Singh
Aron
Author: 6543
Bader Zaidan
Bart Ribbers
Biswapriyo Nath
Bobby The Builder
Boris Dolgov
Brandon Boese
Bryce Vandegrift
Carl Chave
Carlo Landmeter
Carter Li
Celeste
Chleba
Clayton Craft
Coco Liliace
Cormac Stephenson
Cory Sanin
Cowington Post
Craig Andrews
Curt Tilmes
DaKnig
Daniel Fancsali
Daniel Néri
Daniél Kerkmann
Dave Henderson
David Demelier
David Heidelberg
David Sugar
Dekedro
Dennis Krupenik
DerLinkman
Dermot Bradley
Devin Lin
Devon Thyne
Dhruvin Gandhi
Dmitry Klochkov
Dmitry Zakharchenko
Dominique Martinet
Duncan Bellamy
Díaz Urbaneja Víctor Diego Alejandro (Sodomon)
Edd Salkield
Edin Tarić
Eduardo Bart
Elly Fong-Jones
Eric Roshan-Eisner
Fabian Affolter
Fabricio Silva
Faustin Lammler
FintasticMan
Fiona Klute
FollieHiyuki
Francesco Colista
Frank Oltmanns
Fusl
Fxzx mic
Galen Abell
George Hopkins
Glenn Strauss
Guillaume Quintard
Guy Godfroy
Haelwenn (lanodan) Monnier
Hal Martin
Hannes Braun
Henrik Riomar
Hoang Nguyen
Holger Jaekel
Hugo Osvaldo Barrera
Håkan Lindqvist
Iskren Chernev
Iztok Fister Jr
Iztok Fister Jr.
J0WI
Jacob Ludvigsen
Jake Buchholz Göktürk
Jakob Hauser
Jakob Meier
Jakub Jirutka
Jakub Panek
Jaroslav Kysela
Jason Gross
Jason Staten
Jason Swank
Jeff Dickey
Jesse Mandel
Jingyun Hua
Jinming Wu, Patrick
Joel Selvaraj
Johannes Heimansberg
Johannes Marbach
John Anthony
John Gebbie
Jonas
Jonas Vautherin
Jonathan Schleifer
Jordan Christiansen
Joshua Murphy
Jules Maselbas
Julian Groß
Julie Koubova
Jurvis Tan
Justin Berthault
Kaarle Ritvanen
Kaspar Schleiser
Kevin Daudt
Khem Raj
Koni Marti
Konstantin Kulikov
Krassy Boykinov
Krystian Chachuła
Lassebq
Laurent Bercot
Leon Marz
Leon ROUX
Leonardo Arena
Lindsay Zhou
Luca Weiss
Lucidiot
Lukas Franek
Maarten van Gompel
Magnus Sandin
Marcel Steinbeck
Marco Schröder
Marian Buschsieweke
Mark Hills
Marten Ringwelski
Martijn Braam
Marvin Feldmann
Matthias Ahouansou
Michael Pirogov
Michael Truog
Michal Tvrznik
Michał Adamski
Michał Polański
Micheal Smith
Mike Crute
Milan P. Stanić
Miles Alan
Mogens Jensen
Muhammad Adeel
Natanael Copa
Nathan Angelacos
Neale Pickett
NekoCWD
NepNep21
Newbyte
Nicolas Lorin
Nik B
Niklas Meyer
Noah Zalev
Noel Kuntze
Nulo
Oleg Titov
Oliver Smith
Orhun Parmaksız
Pablo Correa Gómez
Patrick Gansterer
Paul Bredbury
Pedro Lucas Porcellis
Peter
Peter Shkenev
Peter van Dijk
Petr Vorel
Phil Estes
Philipp Arras
Qi Xiao
R4SAS
Rabindra Dhakal
Rafael Ávila de Espíndola
Rasmus Thomsen
Raymond Hackley
Ricardo Pchevuzinske Katz
RickyRockRat
Rob Blanckaert
Robert Eckelmann
Robert Mader
Robin Candau
Rudolf Polzer
Sadie Powell
Saijin-Naib
Sam Day
Sam Nystrom
Santurysim
Sascha Brawer
Sean E. Russell
Sean McAvoy
Sebastian Meyer
Sergiy Stupar
Sertonix
Simon Frankenberger
Simon Rupf
Simon Zeni
Siva Mahadevan
Sodface
Stanislav Kholmanskikh
Stefan Hansson
Stefan de Konink
Steffen Nurpmeso
Steve McMaster
Steven Guikal
Sumeet Jhand
Summpot
Sven Wick
Sylvain Prat
Sören Tempel
The one with the braid
Thomas Aldrian
Thomas Böhler
Thomas Deutsch
Thomas J Faughnan Jr
Thomas Kienlen
Thomas Liske
Timo Teräs
Timothy Legge
Tom Lebreux
Tom Wieczorek
Tuan Anh Tran
VehementHam
Vladimir Vitkov
Will Sinatra
William Desportes
William Wilhelm
Willow Barraco
Yann Vigara
Yao Zi
Zach DeCook
Zoey
adamthiede
alealexpro100
bin456789
blacksilver
chimo
crapStone
cristian_ci
dlatchx
donoban
duckl1ng
eletrotupi
famfo
fossdd
geek-at
gs250427
j.r
jahway603
jane400
juef
jvoisin
kamijo
knuxify
kpcyrd
lauren n. liberda
leso-kn
lucidiot
macmpi
michalszmidt
mini-bomba
mio
nezu
nibon7
odrling
omni
ovf
prspkt
psykose
ptrcnull
qaqland
rdbo
rubicon
sewn
shum
sodface
stepech
steve
streaksu
strophy
tetsumaki
timothysteward-wk
u8l
user
wesley van tilburg
xrs
znley
Štěpán Pechman
李通洲
</pre>
