#!/uar/bin/lua

local lyaml = require("lyaml")
local data = lyaml.load(io.read("*a"))
table.sort(data, function(a, b)
	return (a.date or a.updated) > (b.date or b.updated)
end)

local t = {}
for i = 1, 10 do
	t[i] = data[i]
end
io.write(lyaml.dump({ t }))
