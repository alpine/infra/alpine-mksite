FROM alpine

RUN apk update --no-cache && apk add --no-cache \
    busybox-extras \
    curl \
    darkhttpd \
    git \
    lua5.3 \
    lua-rapidjson \
    lua-discount \
    lua-feedparser \
    lua-filesystem \
    lua-lustache \
    lua-lyaml \
    make

WORKDIR /site
COPY . /site

RUN make clean && make -j $(nproc) all

ENTRYPOINT ["darkhttpd", "/site/_out", "--gid", "nobody", "--uid", "nobody"]

CMD ["--port", "8000"]
